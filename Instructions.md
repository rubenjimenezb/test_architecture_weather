# Code Challenge

## Description

The objective of this challenge is to display the weather of a random location defined by its latitude and longitude. In order to generate a random location, please take into account the requirements for valid latitude and a valid longitude.  
Weather information for a specific location can be obtained from the [http://openweathermap.org/current​](http://openweathermap.org/current​) API, using the appId **​0bc9bc2a73fd9644f664cf5f5c5be8d7​**. The request will be done using the weather query by geographic coordinates: [https://openweathermap.org/current#geo​](https://openweathermap.org/current#geo​).

### Requirements

- You have **three days** to complete the challenge.
- The application will consist of one screen showing the weather for the **​current random location** ​obtained from the API. Try to display the information in a meaningful way.
- The latitude and longitude or the name of the city should be displayed too.
- There should be a way to refresh the content by displaying the weather of a new random location.
- Testing is important.
- Provide your code on this git repository.
- Please, include a README describing your solution.
