package com.example.domain.model

data class Weather(
    val lat: Float,
    val lon: Float,
    val city: String,
    val main: String,
    val weatherDescription: String,
    val tem: Float,
    val temMax: Float,
    val temMin: Float,
    val temFeels: Float,
)
