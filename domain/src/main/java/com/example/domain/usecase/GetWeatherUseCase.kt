package com.example.domain.usecase

import com.example.domain.commons.Resource
import com.example.domain.model.Weather
import com.example.domain.repository.WeatherRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.io.IOException
import javax.inject.Inject

class GetWeatherUseCase @Inject constructor(
    private val repository: WeatherRepository
) {
    operator fun invoke(lat: Float, lon: Float): Flow<Resource<Weather>> = flow {
        try {
            emit(Resource.Loading())
            val weather = repository.getWeather(lat = lat, lon = lon)
            emit(Resource.Success(weather))
        } catch (e: Exception) {
            emit(Resource.Error(e.localizedMessage))
        } catch (e: IOException) {  // to get internet or connection related expections
            emit(Resource.Error("Please Check your Internet"))
        }
    }
}