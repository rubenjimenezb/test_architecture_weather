package com.example.fevercodechallenge.utils

import java.util.*

object CoordinateUtils {
    const val LAT_START_VALUE = -90.0f
    const val LAT_END_VALUE = 90.0f
    const val LON_START_VALUE = -180.0f
    const val LON_END_VALUE = 180.0f

    private fun getRandomNumber(start: Float, end: Float) : Float {
        val r = Random()
        return start + r.nextFloat() * (end - start)
    }

    fun getRandomLat() : Float {
        return getRandomNumber(start = LAT_START_VALUE, end = LAT_END_VALUE)
    }

    fun getRandomLon() : Float {
        return getRandomNumber(start = LON_START_VALUE, end = LON_END_VALUE)
    }
}