package com.example.fevercodechallenge.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.content.res.AppCompatResources
import androidx.lifecycle.viewModelScope
import com.example.domain.model.Weather
import com.example.fevercodechallenge.R
import com.example.fevercodechallenge.databinding.ActivityMainBinding
import com.example.fevercodechallenge.navigation.AppDialogNavigator
import com.example.fevercodechallenge.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val mainViewModel: MainViewModel by viewModels()

    private lateinit var binding: ActivityMainBinding

    @Inject
    lateinit var appDialogNavigator: AppDialogNavigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initViewModel()

        initListeners()
    }

    private fun initListeners() {
        binding.apply {
            fabRefresh.setOnClickListener { mainViewModel.getNewWeather() }
        }
    }

    private fun initViewModel() {

        mainViewModel.getNewWeather()

        mainViewModel.weatherState.onEach { result ->
            if (result.error.isEmpty()) {
                if (result.isLoading) {
                    showLoading()
                } else {
                    hideLoading()
                    bindDetail(result.weather)
                }
            } else {
                hideLoading()
                showError(result.error)
            }
        }.launchIn(mainViewModel.viewModelScope)
    }

    private fun showError(extraError: String) {
        appDialogNavigator.showError(
            extraDescription = extraError,
            context = this,
            onRetry = {
                mainViewModel.retry()
            }
        )

    }

    private fun showLoading() {
        appDialogNavigator.showProgressDialog(this)
    }

    private fun hideLoading() {
        appDialogNavigator.hideCurrentProgressDialog()
    }

    private fun bindDetail(weather: Weather?) {
        binding.apply {
            tvCityExplanation.text = getString(
                R.string.city_explanation,
                weather?.lat?.toString() ?: "",
                weather?.lon?.toString() ?: "",
            )
            tvTemp.text = weather?.tem?.toString() ?: ""
            tvTempExplanation.text = getString(
                R.string.temp_explanation,
                weather?.temMin?.toString() ?: "",
                weather?.temMax?.toString() ?: "",
                weather?.temFeels?.toString() ?: ""
            )
            tvCity.text =
                if (weather?.city.isNullOrEmpty()) getString(R.string.possibly_water) else weather?.city
            tvDescription.text = weather?.weatherDescription ?: ""
            clMain.background = AppCompatResources.getDrawable(
                applicationContext, when (weather?.main) {
                    "Clear" -> R.drawable.background_sunny
                    "Clouds" -> R.drawable.background_cloud
                    "Rain" -> R.drawable.background_rain
                    else -> R.drawable.background_default
                }
            )

        }
    }
}