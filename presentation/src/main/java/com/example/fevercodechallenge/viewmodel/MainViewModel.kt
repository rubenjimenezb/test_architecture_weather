package com.example.fevercodechallenge.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.commons.Resource
import com.example.domain.usecase.GetWeatherUseCase
import com.example.fevercodechallenge.utils.CoordinateUtils
import com.example.fevercodechallenge.utils.EspressoIdlingResource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val getWeatherUseCase: GetWeatherUseCase

) : ViewModel() {

    private val _weatherState = MutableStateFlow(WeatherState())
    val weatherState = _weatherState.asStateFlow()

    private var lastLat : Float = 0.0f
    private var lastLon : Float = 0.0f

    fun getNewWeather() {
        lastLat = CoordinateUtils.getRandomLat()
        lastLon = CoordinateUtils.getRandomLon()
        callGetWeather()
    }

    private fun callGetWeather() {
        EspressoIdlingResource.increment()
        getWeatherUseCase(lat = lastLat, lon = lastLon).onEach { result ->
            when (result) {
                is Resource.Success -> {
                    _weatherState.value = WeatherState(weather = result.data)
                    EspressoIdlingResource.decrement()
                }
                is Resource.Error -> {
                    _weatherState.value = WeatherState(error = result.message ?: "An unexpected error occurred")
                    EspressoIdlingResource.decrement()
                }
                is Resource.Loading -> {
                    _weatherState.value = WeatherState(isLoading = true)
                }
            }
        }.launchIn(viewModelScope)    }

    fun retry() {
        callGetWeather()
    }
}