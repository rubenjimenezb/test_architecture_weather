package com.example.fevercodechallenge.viewmodel

import com.example.domain.model.Weather

data class WeatherState(
    val isLoading : Boolean = false,
    val weather : Weather? = null,
    val error : String = ""
)