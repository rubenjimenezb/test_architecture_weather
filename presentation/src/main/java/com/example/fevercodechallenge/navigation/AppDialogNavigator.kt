package com.example.fevercodechallenge.navigation

import android.app.Dialog
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.appcompat.app.AlertDialog
import com.example.fevercodechallenge.R
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppDialogNavigator @Inject constructor() {
    /**
     * Current dialog
     */
    private var currentProgressDialog: Dialog? = null

    /**
     * Open progress dialog
     */
    fun showProgressDialog(context: Context) {
        currentProgressDialog?.cancel()

        // Show alert in main thread
        val mainHandler = Handler(Looper.getMainLooper())
        mainHandler.post { //inflate view
            val flLoading = LayoutInflater.from(context)
                .inflate(R.layout.lay_full_screen_view_loader, null) as FrameLayout

            // Set up dialog
            val dialog = Dialog(context, R.style.LoadingTheme)
            dialog.setContentView(flLoading)
            dialog.setCancelable(false)
            dialog.setCanceledOnTouchOutside(false)

            // Show dialog
            currentProgressDialog = dialog
            currentProgressDialog!!.show()
        }
    }

    /**
     * Hide current dialog from screen
     */
    fun hideCurrentProgressDialog() {
        try {
            if (currentProgressDialog != null && currentProgressDialog!!.isShowing) {
                currentProgressDialog!!.dismiss()
            }
            currentProgressDialog = null
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            currentProgressDialog = null
        }
    }

    fun showError(
        extraDescription: String = "",
        context: Context,
        onRetry: (() -> Unit)? = null,
    ) {
        val message =
            context.getString(R.string.alert_description) + if (!extraDescription.isNullOrEmpty()) "\n$extraDescription" else ""
        AlertDialog.Builder(context)
            .setTitle(R.string.alert_title)
            .setMessage(message)
            .setPositiveButton(R.string.alert_retry) { dialog, _ ->
                dialog.cancel()
                onRetry?.invoke()
            }
            .setNegativeButton(R.string.alert_cancel) { dialog, _ ->
                dialog.cancel()
            }
            .setCancelable(true)
            .show()
    }
}