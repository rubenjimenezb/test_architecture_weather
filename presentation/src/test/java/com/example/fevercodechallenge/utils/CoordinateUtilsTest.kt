package com.example.fevercodechallenge.utils

import org.junit.Assert
import org.junit.Test

class CoordinateUtilsTest {

    @Test fun test_randomLat() {
        for (i in 1..200){
            val lat = CoordinateUtils.getRandomLat()
            Assert.assertTrue(lat >= CoordinateUtils.LAT_START_VALUE && lat <= CoordinateUtils.LAT_END_VALUE)
        }
    }

    @Test fun test_randomLon() {
        for (i in 1..200){
            val lon = CoordinateUtils.getRandomLon()
            Assert.assertTrue(lon >= CoordinateUtils.LON_START_VALUE && lon <= CoordinateUtils.LON_END_VALUE)
        }
    }
}