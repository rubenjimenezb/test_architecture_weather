package com.example.fevercodechallenge.utils

/**
 * Idling Resource for UI Tests in Release.
 * We left all the methods empty, we don´t want release version to have changes related to unit testing.
 */
object EspressoIdlingResource {
    fun increment() {}

    fun decrement() {}
}