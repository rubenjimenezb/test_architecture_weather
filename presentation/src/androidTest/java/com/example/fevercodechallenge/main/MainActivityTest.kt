package com.example.fevercodechallenge.main

import androidx.test.espresso.Espresso.*
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.*
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.example.fevercodechallenge.R
import com.example.fevercodechallenge.util.EspressoIdlingResourceRule
import com.example.fevercodechallenge.utils.CoordinateUtils
import io.mockk.every
import io.mockk.mockkObject
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class MainActivityTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @get: Rule
    val espressoIdlingResoureRule = EspressoIdlingResourceRule()

    @Before
    fun setup() {
        //Mock random coordinates generator to be a concrete city
        mockkObject(CoordinateUtils)
        every {
            CoordinateUtils.getRandomLat()
        } returns EXPECTED_LAT
        every {
            CoordinateUtils.getRandomLon()
        } returns EXPECTED_LON
    }

    @Test
    fun a_test_isMainActivityVisible_onAppLaunch() {
        onView(withId(R.id.clMain))
            .check(matches(isDisplayed()))
    }

    @Test
    fun a_test_isRefreshButtonWorking() {
        onView(withId(R.id.clMain))
            .check(matches(isDisplayed()))

        onView(withId(R.id.fabRefresh))
            .perform(click())

        onView(withId(R.id.tvCity))
            .check(matches(withText(EXPECTED_CITY)))
    }

    companion object {
        const val EXPECTED_LAT = 40.30571f
        const val EXPECTED_LON = -3.73295f
        const val EXPECTED_CITY = "Getafe"
    }
}