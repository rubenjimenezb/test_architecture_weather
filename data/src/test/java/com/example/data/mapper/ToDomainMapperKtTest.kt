package com.example.data.mapper

import com.example.data.model.CoordDto
import com.example.data.model.MainDto
import com.example.data.model.WeatherDto
import com.example.data.model.WeatherObjDto
import org.junit.Assert
import org.junit.Test

class ToDomainMapperKtTest {

    @Test
    fun test_weatherMapper() {
        val weatherDto = WeatherDto(
            coord = CoordDto(
                lat = EXPECTED_LAT.toDouble(),
                lon = EXPECTED_LON.toDouble()
            ),
            name = EXPECTED_CITY,
            weather = arrayListOf(
                WeatherObjDto(
                    main = EXPECTED_MAIN,
                    description = EXPECTED_WEATHER_DESCRIPTION
                )
            ),
            main = MainDto(
                temp = EXPECTED_TEM.toDouble(),
                tempMax = EXPECTED_TEM_MAX.toDouble(),
                tempMin = EXPECTED_TEM_MIN.toDouble(),
                feelsLike = EXPECTED_TEM_FEELS.toDouble(),
            )
        )
        //Make the map to Weather domain object
        val weatherMapped = weatherDto.toDomain()

        //Make sure all fields were mapped correctly
        Assert.assertTrue(weatherMapped.lat == EXPECTED_LAT)
        Assert.assertTrue(weatherMapped.lon == EXPECTED_LON)
        Assert.assertTrue(weatherMapped.city == EXPECTED_CITY)
        Assert.assertTrue(weatherMapped.main == EXPECTED_MAIN)
        Assert.assertTrue(weatherMapped.weatherDescription == EXPECTED_WEATHER_DESCRIPTION)
        Assert.assertTrue(weatherMapped.tem == EXPECTED_TEM)
        Assert.assertTrue(weatherMapped.temMax == EXPECTED_TEM_MAX)
        Assert.assertTrue(weatherMapped.temMin == EXPECTED_TEM_MIN)
        Assert.assertTrue(weatherMapped.temFeels == EXPECTED_TEM_FEELS)
    }

    companion object {
        const val EXPECTED_LAT = 0.5f
        const val EXPECTED_LON = 80.5f
        const val EXPECTED_CITY = "Getafe"
        const val EXPECTED_MAIN = "Cloud"
        const val EXPECTED_WEATHER_DESCRIPTION = "Very cloudy"
        const val EXPECTED_TEM = 26.5f
        const val EXPECTED_TEM_MAX = 30.5f
        const val EXPECTED_TEM_MIN = 20.5f
        const val EXPECTED_TEM_FEELS = 42.5f
    }

}