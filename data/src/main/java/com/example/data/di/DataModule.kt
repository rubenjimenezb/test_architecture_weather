package com.example.data.di

import com.example.data.datasource.WeatherRemoteDataSource
import com.example.data.repository.WeatherRepositoryImpl
import com.example.data.service.WEATHER_BASE_URL
import com.example.data.service.WEATHER_DEFAULT_TIMEOUT
import com.example.data.service.WeatherApi
import com.example.domain.repository.WeatherRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {

    @Provides
    @Singleton
    fun provideWeatherRepository(
        weatherRemoteDataSource: WeatherRemoteDataSource
    ): WeatherRepository {
        return WeatherRepositoryImpl(
            weatherRemoteDataSource = weatherRemoteDataSource,
        )
    }

    @Provides
    @Singleton
    fun provideWeatherApi(
        @WeatherApiUrl url: String,
        @HttpClient client: OkHttpClient
    ): WeatherApi {

        val retrofit = Retrofit.Builder()
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(url)
            .build()

        return retrofit.create(WeatherApi::class.java)
    }

    @Provides
    @Singleton
    @WeatherApiUrl
    fun provideWeatherApiUrl(): String =
        WEATHER_BASE_URL

    @Provides
    @Singleton
    @HttpClient
    fun provideOkHttpClient(
        @HttpLogInterceptor interceptor: HttpLoggingInterceptor
    ): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(WEATHER_DEFAULT_TIMEOUT, TimeUnit.SECONDS)
            .addInterceptor(interceptor)
            .build()

    @Provides
    @Singleton
    @HttpLogInterceptor
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }
}
