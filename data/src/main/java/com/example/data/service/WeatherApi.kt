package com.example.data.service

import com.example.data.model.WeatherDto
import retrofit2.http.POST
import retrofit2.http.Query

interface WeatherApi {
    @POST(WEATHER_SERVICE_ENDPOINT)
    suspend fun getWeather(
        @Query(WEATHER_LAT_KEY) lat: Float,
        @Query(WEATHER_LON_KEY) lon: Float,
        @Query(WEATHER_API_ID_KEY) appId: String = WEATHER_APP_ID,
    ): WeatherDto
}