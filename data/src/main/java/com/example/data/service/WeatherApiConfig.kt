package com.example.data.service

const val WEATHER_BASE_URL = "https://api.openweathermap.org/"
//Changed Api key, the other one was giving a 401
const val WEATHER_APP_ID = "e399324e4d66848425a9e3c5551944e5"
const val WEATHER_DEFAULT_TIMEOUT = 10L
const val WEATHER_SERVICE_ENDPOINT = "data/2.5/weather"
const val WEATHER_LAT_KEY = "lat"
const val WEATHER_LON_KEY = "lon"
const val WEATHER_API_ID_KEY = "appid"


