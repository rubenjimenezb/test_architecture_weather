package com.example.data.mapper

import com.example.data.model.WeatherDto
import com.example.domain.model.Weather

fun WeatherDto.toDomain(): Weather {
    return Weather(
        lat = this.coord?.lat?.toFloat() ?: 0.0f,
        lon = this.coord?.lon?.toFloat() ?: 0.0f,
        city = this.name ?: "",
        main = this.weather.firstOrNull()?.main ?: "",
        weatherDescription = this.weather.firstOrNull()?.description ?: "",
        tem = this.main?.temp?.toFloat() ?: 0.0f,
        temMax = this.main?.tempMax?.toFloat() ?: 0.0f,
        temMin = this.main?.tempMin?.toFloat() ?: 0.0f,
        temFeels = this.main?.feelsLike?.toFloat() ?: 0.0f,
    )
}