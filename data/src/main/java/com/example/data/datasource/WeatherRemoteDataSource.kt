package com.example.data.datasource

import com.example.data.mapper.toDomain
import com.example.data.service.WeatherApi
import com.example.domain.model.Weather
import javax.inject.Inject

class WeatherRemoteDataSource @Inject constructor(
    private val api: WeatherApi,
) {
    suspend fun getWeather(lat: Float, lon: Float): Weather {
        return api.getWeather(lat = lat, lon = lon).toDomain()
    }
}