package com.example.data.repository

import com.example.data.datasource.WeatherRemoteDataSource
import com.example.domain.model.Weather
import com.example.domain.repository.WeatherRepository
import javax.inject.Inject

class WeatherRepositoryImpl @Inject constructor(
    private val weatherRemoteDataSource: WeatherRemoteDataSource
) : WeatherRepository {
    override suspend fun getWeather(lat: Float, lon: Float): Weather {
        return weatherRemoteDataSource.getWeather(lat = lat, lon = lon)
    }
}