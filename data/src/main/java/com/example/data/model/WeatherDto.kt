package com.example.data.model

import com.google.gson.annotations.SerializedName

data class WeatherDto(
    val coord: CoordDto? = CoordDto(),
    val weather: ArrayList<WeatherObjDto> = arrayListOf(),
    val base: String? = null,
    val main: MainDto? = MainDto(),
    val visibility: Int? = null,
    val wind: WindDto? = WindDto(),
    val clouds: CloudsDto? = CloudsDto(),
    val dt: Int? = null,
    val sys: SysDto? = SysDto(),
    val timezone: Int? = null,
    val id: Int? = null,
    val name: String? = null,
    val cod: Int? = null
)

data class CoordDto(
    val lon: Double? = null,
    val lat: Double? = null
)

data class WeatherObjDto(
    val id: Int? = null,
    val main: String? = null,
    val description: String? = null,
    val icon: String? = null
)

data class MainDto(
    val temp: Double? = null,
    @SerializedName("feels_like") val feelsLike: Double? = null,
    @SerializedName("temp_min") val tempMin: Double? = null,
    @SerializedName("temp_max") val tempMax: Double? = null,
    val pressure: Int? = null,
    val humidity: Int? = null
)

data class WindDto(
    val speed: Double? = null,
    val deg: Int? = null
)

data class CloudsDto(
    val all: Int? = null
)

data class SysDto(
    val type: Int? = null,
    val id: Int? = null,
    val message: Double? = null,
    val country: String? = null,
    val sunrise: Int? = null,
    val sunset: Int? = null
)