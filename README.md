# TestArchitecture

Fever Code Challenge is a project made to develop a good architecture


## Architecture

The architecture used is MVVM and it has the following main components.
- Views: (Activities and fragments) who observe the changes from flow state variables in view model and paint the screen. And also receive user input.
- ViewModels: It is where are all the controls for interacting with the View. It contains some variables to be observed by Views.
- UseCases: The UseCase are classes to do a determinate task. It connects with repositories.
- Repository: They handle the access to the different sources of information. In this app we only have one different data source: Remote.
- DataSource: They get the data from a service to a remote api.


## Libraries used

- [ ] [Hilt](https://developer.android.com/training/dependency-injection/hilt-android?hl=es-419) We use Hilt for dependency injection in the app.
- [ ] [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) Stores and handle the data related to a UI in an improved way.
- [ ] [StateFlow](https://developer.android.com/kotlin/flow/stateflow-and-sharedflow?hl=es-419) Flow to update the view when the object changes..
- [ ] [Retrofit](https://github.com/square/retrofit) Retrofit is a library to handle service petitions.
- [ ] [KotlinCoroutines](https://kotlinlang.org/docs/coroutines-overview.html) For managing background threads with simplified code and reducing needs for callbacks.


## Some unit testing and UI testing added.

- Unit testing in data layer to test if the toDomainMapper function is mapping correctly
- Unit testing in presentation layer to test if the random coordinate generator is generating latitudes and longitudes correctly.
- UI testing in presentation layer with a mock for the coordinates that checks if the screen is updated with the data for that coordinates 


## ToDo list. Things that I have not had time to develop yet.

- Improve the mock in UI testing to be able to test mocking directly the full response of the service, being able then to test all the fields in the screen.
- Maybe store in room the last Weather received to show it in case there isn't Internet connection. Offline first.
- Maybe store in room all the results to paint a list of all recent results to allow acceding to them. Optional.
- Do more tests to cover a bigger part of the app. 
